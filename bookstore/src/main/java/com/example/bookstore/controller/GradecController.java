package com.example.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Grade;
import com.example.bookstore.repository.GradeRepository;

@RestController
@RequestMapping("/api/grade")
public class GradecController {
	
	@Autowired
    GradeRepository gradeRepository;

    // Get All Notes
    @GetMapping("/get")
    public List<Grade> getAllGrades() {
        return gradeRepository.findAll();
    }

    // Create a new Note
    @PostMapping("/create")
    public Grade createGrade(@Valid @RequestBody Grade grade) {
        return gradeRepository.save(grade);
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Grade getGradeById(@PathVariable(value = "id") Long gradeId) {
        return gradeRepository.findById(gradeId).get();
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Grade updateGrade(@PathVariable(value = "id") Long gradeId,
                                            @Valid @RequestBody Grade gradeDetails) {

    	Grade grade = gradeRepository.findById(gradeId).get();

        grade.setQuality(gradeDetails.getQuality());
        grade.setBaseProduction(gradeDetails.getBaseProduction());

        Grade updatedGrade = gradeRepository.save(grade);
        return updatedGrade;
    }
    
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteGrade(@PathVariable(value = "id") Long gradeId) {
    	Grade grade = gradeRepository.findById(gradeId).get();

    	gradeRepository.delete(grade);

        return ResponseEntity.ok().build();
    }
}
