package com.example.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Author;
import com.example.bookstore.repository.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
	
	  @Autowired
	    AuthorRepository authorRepository;
	    
	    // Get All Notes
	    @GetMapping("/get")
	    public List<Author> getAllAuthors() {
	        return authorRepository.findAll();
	    }
	    
	    // Create a new Note
	    @PostMapping("/create")
	    public Author createAuthor(@Valid @RequestBody Author author) {
	        return authorRepository.save(author);
	    }
	    
	    // Get a Single Note
	    @GetMapping("/getsingle/{id}")
	    public Author getAuthorById(@PathVariable(value = "id") Long authorId) {
	        return authorRepository.findById(authorId).get();
	                
	    }
	    
	    // Update a Note
	    @PutMapping("/update/{id}")
	    public Author updateAuthor(@PathVariable(value = "id") Long authorId,
	                                            @Valid @RequestBody Author authorDetails) {

	    	Author author = authorRepository.findById(authorId).get();

	    	author.setFirstName(authorDetails.getFirstName());
	    	author.setLastName(authorDetails.getLastName());

	    	Author updatedAuthor = authorRepository.save(author);
	        return updatedAuthor;
	    }
	    
	    // Delete a Note
	    @DeleteMapping("/delete/{id}")
	    public ResponseEntity<?> deleteAuthor(@PathVariable(value = "id") Long authorId) {
	    	Author author = authorRepository.findById(authorId).get();

	    	authorRepository.delete(author);

	        return ResponseEntity.ok().build();
	    }
}
