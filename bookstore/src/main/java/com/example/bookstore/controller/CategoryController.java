package com.example.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Category;
import com.example.bookstore.repository.CategoryRepository;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

	@Autowired
	CategoryRepository categoryRepository;
    
    // Get All Notes
    @GetMapping("/get")
    public List<Category> getAllCategory() {
        return categoryRepository.findAll();
    }
    
    // Create a new Note
    @PostMapping("/create")
    public Category createCategory(@Valid @RequestBody Category category) {
        return categoryRepository.save(category);
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Category getCategoryById(@PathVariable(value = "id") Long categoryId) {
        return categoryRepository.findById(categoryId).get();
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Category updateCategory(@PathVariable(value = "id") Long categoryId,
                                            @Valid @RequestBody Category categoryDetails) {

    	Category category = categoryRepository.findById(categoryId).get();

    	category.setCategoryName(categoryDetails.getCategoryName());
    	category.setCategoryDescription(categoryDetails.getCategoryDescription());

    	Category updatedCategory = categoryRepository.save(category);
        return updatedCategory;
    }
    
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable(value = "id") Long categoryId) {
    	Category category = categoryRepository.findById(categoryId).get();

    	categoryRepository.delete(category);

        return ResponseEntity.ok().build();
    }
}
