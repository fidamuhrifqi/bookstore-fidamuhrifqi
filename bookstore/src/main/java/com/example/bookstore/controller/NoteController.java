package com.example.bookstore.controller;

import com.example.bookstore.model.Notes;
import com.example.bookstore.repository.NotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/notes")
public class NoteController {

    @Autowired
    NotesRepository notesRepository;

    // Get All Notes
    @GetMapping("/get")
    public List<Notes> getAllNotes() {
        return notesRepository.findAll();
    }

    // Create a new Note
    @PostMapping("/create")
    public Notes createNote(@Valid @RequestBody Notes note) {
        return notesRepository.save(note);
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Notes getNoteById(@PathVariable(value = "id") Long noteId) {
        return notesRepository.findById(noteId).get();
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Notes updateNote(@PathVariable(value = "id") Long noteId,
                                            @Valid @RequestBody Notes noteDetails) {

        Notes note = notesRepository.findById(noteId).get();

        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());

        Notes updatedNote = notesRepository.save(note);
        return updatedNote;
    }
    
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
        Notes note = notesRepository.findById(noteId).get();
        notesRepository.delete(note);

        return ResponseEntity.ok().build();
    }
}