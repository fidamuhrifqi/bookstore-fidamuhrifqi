package com.example.bookstore.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Books;
import com.example.bookstore.repository.BooksRepository;

@RestController
@RequestMapping("/api/books")
public class BooksController {
	
    @Autowired
    BooksRepository booksRepository;
    
    // Get All Notes
    @GetMapping("/get")
    public List<Books> getAllBooks() {
        return booksRepository.findAll();
    }
    
    // Create a new Note
    @PostMapping("/create")
    public Books createBooks(@Valid @RequestBody Books books) {
        return booksRepository.save(books);
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Books getBooksById(@PathVariable(value = "id") Long booksId) {
        return booksRepository.findById(booksId).get();
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Books updateBooks(@PathVariable(value = "id") Long booksId,
                                            @Valid @RequestBody Books booksDetails) {

        Books books = booksRepository.findById(booksId).get();

        books.setTitle(booksDetails.getTitle());
        books.setDescription(booksDetails.getDescription());
        books.setReleaseDate(booksDetails.getReleaseDate());
        books.setAuthor(booksDetails.getAuthor());
        books.setPublisher(booksDetails.getPublisher());
        books.setCategory(booksDetails.getCategory());

        Books updatedBooks = booksRepository.save(books);
        return updatedBooks;
    }
    
    // Delete a Note
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBooks(@PathVariable(value = "id") Long booksId) {
    	Books books = booksRepository.findById(booksId).get();

    	booksRepository.delete(books);

        return ResponseEntity.ok().build();
    }
}
