package com.example.bookstore.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookstore.model.Books;
import com.example.bookstore.repository.BooksRepository;

@RestController
@RequestMapping("/api/calculation/book/price")
public class CalculationBookPriceController {
	
	@Autowired
    BooksRepository booksRepository;

    // Update a Book Price
    @PutMapping("/update")
    public HashMap<String, Object> calculationBookPrice() {
    	
    	List<Books> getAllBooks = booksRepository.findAll();
    	
    	for (Books booksFromList : getAllBooks) {
    		
    		Books book = booksFromList;
            double bookPrice = 0, taxFinal = 0, finalPrice, basePrice;
            int releaseYear, currentYear;
            
            releaseYear = book.getReleaseDate().getYear();
            currentYear = Calendar.getInstance().get(Calendar.YEAR);
            basePrice = book.getPublisher().getGrade().getBaseProduction();

            if (releaseYear == currentYear) {
            	bookPrice = calculationRatePrice(basePrice, 1.5);
            }
            else {
            	bookPrice = calculationRatePrice(basePrice, 1.3);
			}
            
            if (book.getPublisher().getCountry().equalsIgnoreCase("Indonesia")) {
				taxFinal = calculationTax(basePrice, 0.05);
			}
            else {
            	taxFinal = calculationTax(basePrice, 0.1);
			}
            
            finalPrice = taxFinal + bookPrice;
            
            book.setPrice(finalPrice);
            booksRepository.save(book);
		}
    	
    	HashMap<String, Object> message = new HashMap<String, Object>();
    	message.put("Status", 200);
    	message.put("Message", "BookPrice Calculation Is Success");

    	
		return message;
    }

    public double calculationRatePrice(double price, double ratePrice) {
    	double finalPrice = ratePrice * price;
    	return finalPrice;
    }
    
	public double calculationTax(double price, double tax) {
		double finalTax = tax * price;
		return finalTax;	
	}
}
